package com.haein.tone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class ResultActivity extends AppCompatActivity {

    Button btn_result;
    private String TAG = ResultActivity.class.getSimpleName();
    private int res_drawables[] = {R.drawable.result_spring, R.drawable.result_summer, R.drawable.result_autumn, R.drawable.result_winter};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        btn_result = (Button) findViewById(R.id.btn_result);

        btn_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, MenuActivity.class);
                startActivity(intent);
            }
        });

        String result_path = LoginActivity.PERsONAL_COLOR_DBHELPER.selectAlbum(LoginActivity.ID);
        Log.i(TAG, "onCreate: " + result_path);
        String result_image[] = result_path.split(",");

        int spring=0, summer = 0, autumn = 0, winter = 0;

        for(int i=0; i<result_image.length; i++) {

            if(result_image[i].contains("spring")) {
                spring++;
            } else if(result_image[i].contains("summer")) {
                summer++;
            } else if(result_image[i].contains("autumn")) {
                autumn++;
            } else if(result_image[i].contains("winter")) {
                winter++;
            }
        }

        int array[] = {spring, summer, autumn, winter};
        int res=0;
        for (int i = 0; i < array.length; i++) {
            if (array[res] < array[i]) {
                res = i;
            }
        }

        Log.i(TAG, "onCreate: "+res);

        ImageView tone_result = findViewById(R.id.tone_result);

        tone_result.setBackground(getDrawable(res_drawables[res]));

        LoginActivity.PERsONAL_COLOR_DBHELPER.updateResult(LoginActivity.ID, res);

    }



}
