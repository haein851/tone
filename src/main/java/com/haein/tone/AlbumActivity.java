package com.haein.tone;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.haein.tone.Album.AlbumAdapter;

import java.util.Arrays;

import static com.haein.tone.LoginActivity.PERsONAL_COLOR_DBHELPER;

public class AlbumActivity extends AppCompatActivity {

    private AlbumAdapter albumAdapter;
    private final String TAG = AlbumActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        RecyclerView recyclerView = findViewById(R.id.gridview_album);
        String picturePath = PERsONAL_COLOR_DBHELPER.selectAlbum(LoginActivity.ID);
        String[] picturePathes = picturePath.replace("[","").replace("]","").split(", ");
        albumAdapter = new AlbumAdapter(this, Arrays.asList(picturePathes));
        recyclerView.setAdapter(albumAdapter);
    }
}
