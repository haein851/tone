package com.haein.tone;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.haein.tone.Makeup.MakeupAdapter;
import com.haein.tone.Makeup.ProductObject;

import java.util.ArrayList;

public class MakeupActivity extends AppCompatActivity {

    private ArrayList<Integer> seasonDrawable = new ArrayList<>();

    private ArrayList<ProductObject> productObjs = new ArrayList<>();
    private RecyclerView recycle_makeupList;
    private int[] season = {16,14,18,16};
    private String TAG = MakeupActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makeup);

        int res = LoginActivity.PERsONAL_COLOR_DBHELPER.toneResult(LoginActivity.ID);
        String resStr = "";
        if(res ==0) resStr="spring";
        else if(res == 1) resStr ="summer";
        else if(res == 2) resStr = "autumn";
        else if(res ==3) resStr = "winter";
        for (int i = 1; i <= season[res]; i++) {
            int id = getResources().getIdentifier(resStr +"_makeup_" + i, "drawable", getPackageName());
            seasonDrawable.add(id);
        }
        recycle_makeupList = findViewById(R.id.recycle_makeupList);

        int array = getResources().getIdentifier(resStr +"_makeup", "array", getPackageName());
        String[] makeupList = getResources().getStringArray(array);
        for(int i=0;i<makeupList.length;i++){
            ProductObject productObject = new ProductObject();

            productObject.drawable = seasonDrawable.get(i);
            Log.i(TAG, "onCreate: "+makeupList[i]);
            String[] makeup = makeupList[i].split("%");
            productObject.name = makeup[0];
            productObject.brand = makeup[1];
            productObject.price = makeup[2];
            productObjs.add(productObject);
        }

        MakeupAdapter makeupAdapter= new MakeupAdapter(this, productObjs);
        recycle_makeupList.setAdapter(makeupAdapter);
    }
}
