package com.haein.tone.Makeup;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.haein.tone.Album.AlbumAdapter;
import com.haein.tone.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MakeupAdapter extends RecyclerView.Adapter<MakeupAdapter.ViewHolder> {
private final String TAG = MakeupAdapter.class.getSimpleName();
    private final Context context;
    private final ArrayList<ProductObject> productObjects;

    public MakeupAdapter(Context context, ArrayList<ProductObject> productObjects) {
        this.context = context;
        this.productObjects = productObjects;
    }

    @NonNull
    @Override
    public MakeupAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_makup, parent,false);

        return new MakeupAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MakeupAdapter.ViewHolder holder, int position) {
        holder.makeup_image.setBackgroundResource(productObjects.get(position).drawable);
        holder.makeup_name.setText(productObjects.get(position).name);
        holder.makeup_brand.setText(productObjects.get(position).brand);
        holder.makeup_price.setText(productObjects.get(position).price);
        Log.i(TAG, "onBindViewHolder: "+productObjects.get(position).name);
        Log.i(TAG, "onBindViewHolder: "+productObjects.get(position).brand);
        Log.i(TAG, "onBindViewHolder: "+productObjects.get(position).price);
    }

    @Override
    public int getItemCount() {
        return productObjects.size();
    }

     class ViewHolder extends RecyclerView.ViewHolder {

        private TextView makeup_name, makeup_brand, makeup_price;
        private ImageView makeup_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            makeup_image = itemView.findViewById(R.id.makeup_image);
            makeup_name = itemView.findViewById(R.id.makeup_name);
            makeup_brand = itemView.findViewById(R.id.makeup_brand);
            makeup_price = itemView.findViewById(R.id.makeup_price);
        }
    }
}
