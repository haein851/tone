package com.haein.tone.Album;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.haein.tone.CameraActivity;
import com.haein.tone.LoginActivity;
import com.haein.tone.R;
import com.haein.tone.ResultActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder> {
    private final String TAG = AlbumAdapter.class.getSimpleName();
    private final Activity context;
    private List<String> drawables;
    public AlbumAdapter(Activity context, List<String> drawables){
        this.drawables = drawables;
        this.context = context;
    }

    //처음 recyclerView가 처음 생성될때 item을 생성해 주는 곳.
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_album, parent,false);

        return new ViewHolder(view);
    }

    //이미 생성된 item에 값만 바꿔 주는 곳.
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.i(TAG, "onBindViewHolder: "+position);
        holder.imageView.setImageBitmap(BitmapFactory.decodeFile(drawables.get(position)));
    }

    @Override
    public int getItemCount() {
        return drawables.size();
    }

    public void setDrawables(ArrayList<String> captureList) {
        this.drawables = captureList;
    }

    //item 한개의 view
    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image_item);

        }

    }

}
