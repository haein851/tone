package com.haein.tone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

public class QuizActivityTwo extends AppCompatActivity {

    Button button_next_two;
    private int cool;
    private int warm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_two);

        button_next_two = (Button) findViewById(R.id.button_next_two);

        cool = getIntent().getIntExtra("cool", 0);
        warm = getIntent().getIntExtra("warm", 0);

        button_next_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioGroup radioGroup = findViewById(R.id.radio_ring);
                int checkedid = radioGroup.getCheckedRadioButtonId();

                switch (checkedid) {
                    case R.id.quiz_silver:
                        cool++;
                        break;
                    case R.id.quiz_gold:
                        warm++;
                        break;
                        default:
                            Toast.makeText(QuizActivityTwo.this, "선택되지 않았습니다", Toast.LENGTH_SHORT).show();

                            return;
                }
                Intent intent = new Intent(QuizActivityTwo.this, QuizActivityThree.class);
                intent.putExtra("cool", cool);
                intent.putExtra("warm", warm);
                startActivity(intent);
            }
        });

    }

}
