package com.haein.tone;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;

public class MenuActivity extends AppCompatActivity {

    Button button_menu_one, button_menu_two, button_menu_three, button_menu_four, button_menu_five;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        button_menu_one = (Button) findViewById(R.id.button_menu_one);
        button_menu_two = (Button) findViewById(R.id.button_menu_two);
        button_menu_five = (Button) findViewById(R.id.button_menu_five);
        button_menu_three = (Button) findViewById(R.id.button_menu_three);
        button_menu_four = (Button) findViewById(R.id.button_menu_four);


        button_menu_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TedPermission.with(getApplicationContext())
                        .setPermissionListener(permissionListener)
                        .setRationaleMessage("카메라 권한이 필요합니다.")
                        .setDeniedMessage("거부하셨습니다.")
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                        .check();


            }
        });

        button_menu_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, AlbumActivity.class);
                startActivity(intent);
            }
        });

        button_menu_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, MakeupActivity.class);
                startActivity(intent);
            }
        });

        button_menu_four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, HairActivity.class);
                startActivity(intent);
            }
        });

        button_menu_five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, QuizActivityOne.class);
                startActivity(intent);
            }
        });


    }

    PermissionListener permissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            Toast.makeText(getApplicationContext(), "권한이 허용됨", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(MenuActivity.this, CameraActivity.class);
            startActivity(intent);
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {

        }
    };


}
