package com.haein.tone.hair;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.haein.tone.Makeup.MakeupAdapter;
import com.haein.tone.Makeup.ProductObject;
import com.haein.tone.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HairAdapter extends RecyclerView.Adapter<HairAdapter.ViewHolder> {

    private final Context context;
    private final ArrayList<ProductObject> productObjects;

    public HairAdapter(Context context, ArrayList<ProductObject> productObjects) {
        this.context = context;
        this.productObjects = productObjects;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_hair, parent,false);

        return new HairAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.hair_image.setBackgroundResource(productObjects.get(position).drawable);
        holder.hair_name.setText(productObjects.get(position).name);
    }

    @Override
    public int getItemCount() {
        return productObjects.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView hair_name;
        private ImageView hair_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            hair_name = itemView.findViewById(R.id.hair_name);
            hair_image = itemView.findViewById(R.id.hair_image);
        }
    }
}
