package com.haein.tone.dbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;

import java.util.ArrayList;

import androidx.annotation.Nullable;

public class PersonalColorDBHelper extends SQLiteOpenHelper {
    // 데이터베이스
    private static final String DATABASE_NAME = "personalColor.db";
    private static final int DATABASE_VERSION = 1;

    // 테이블
    public static final String TABLE_NAME = "userTb";
    private SQLiteDatabase sqLiteDatabase;

    public PersonalColorDBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        sqLiteDatabase=db;
        String sql = "create table userTb (id text primary key, password text, name text, gender text, result INTEGER)";
        sqLiteDatabase.execSQL(sql);

        sql = "CREATE TABLE hairTb (id TEXT, haircolor INTEGER, FOREIGN KEY(id) REFERENCES userTb( id))";
        sqLiteDatabase.execSQL(sql);

        sql = "CREATE TABLE makeupTb (id TEXT, haircolor TEXT, MAKEUPBRAND TEXT, MAKEUPPRICE INTEGER, FOREIGN KEY(id) REFERENCES userTb(id))";
        sqLiteDatabase.execSQL(sql);

        sql = "CREATE TABLE  clothesTb  (id  TEXT, COLTHESNAME  TEXT, COLTHESBRAND  TEXT, COLTHESPRICE  INTEGER, FOREIGN KEY( id ) REFERENCES  userTb ( id ))";
        sqLiteDatabase.execSQL(sql);

        sql = "CREATE TABLE albumTb (id TEXT, picture_path TEXT)";
        sqLiteDatabase.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // 삭제후 다시 생성
//        String sql = "drop table if exists userTb";
//        sqLiteDatabase.execSQL();
//        onCreate(sqLiteDatabase);
    }

    public long insertUser(String id, String password, String name, String gender) {
        ContentValues contentValues = new ContentValues();

        contentValues.put("id", id);
        contentValues.put("password", password);
        contentValues.put("name", name);
        contentValues.put("gender", gender);
        return getWritableDatabase().insert("userTb", null, contentValues);
    }

    public boolean checkinfo(String id, String password) {
        Cursor cursor = getReadableDatabase().query("userTb", new String[]{"id","password"}, "id=?",new String[]{id}, null, null, null);
        if(cursor.moveToNext()) {
            String password_db = cursor.getString(cursor.getColumnIndex("password"));
            return password_db.equals(password);
        }
        return false; //아이디가 없는 경우
    }

    public long insertAlbum(String id, String captureList) {
        ContentValues contentValues = new ContentValues();

        contentValues.put("id", id);

        contentValues.put("picture_path", captureList);
        return getWritableDatabase().insert("albumTb", null, contentValues);
    }

    public String selectAlbum(String id) {
        Cursor cursor = getReadableDatabase().query("albumTb", new String[]{"picture_path"}, "id=?", new String[]{id}, null, null, null);
        if (cursor.moveToNext()) {
            String picture_path = cursor.getString(cursor.getColumnIndex("picture_path"));
            return picture_path;
        }
        return null;
    }

    public long deleteAlbum(String id) {
        ContentValues contentValues = new ContentValues();

        contentValues.put("id", id);
        return getWritableDatabase().delete("albumTb", "id=?", new String[] {id});
    }

    public long updateResult(String id, int result) {
        ContentValues contentValues = new ContentValues();

        contentValues.put("result", result);
        return getWritableDatabase().update("userTb", contentValues, "id=?", new String[] {id});
    }

    public int toneResult(String id) {
        Cursor cursor = getReadableDatabase().query("userTb", new String[] {"result"}, "id=?", new String[] {id}, null, null, null);
        cursor.moveToNext();
        return cursor.getInt(cursor.getColumnIndex("result"));
    }

}
