package com.haein.tone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.haein.tone.dbHelper.PersonalColorDBHelper;

public class LoginActivity extends Activity {

    public static PersonalColorDBHelper PERsONAL_COLOR_DBHELPER;
    public static String ID;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText idText = (EditText) findViewById(R.id.idText);
        final EditText passwordText = (EditText) findViewById(R.id.passwordText);
        Button loginButton = (Button) findViewById(R.id.loginButton);
        TextView registerButton = (TextView) findViewById(R.id.registerButton);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                LoginActivity.this.startActivity(registerIntent);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isLogin=PERsONAL_COLOR_DBHELPER.checkinfo(idText.getText().toString(), passwordText.getText().toString());
                if(isLogin){
                    ID=idText.getText().toString();
                    startActivity(new Intent(LoginActivity.this, MenuActivity.class));
                } else {
                    Toast.makeText(getApplicationContext(), "아이디/비밀번호 틀림", Toast.LENGTH_SHORT).show();
                }
            }
        });

        PERsONAL_COLOR_DBHELPER = new PersonalColorDBHelper(getApplicationContext(), "tone.db", null, 1);
    }
}
