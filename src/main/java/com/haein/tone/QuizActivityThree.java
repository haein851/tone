package com.haein.tone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

public class QuizActivityThree extends AppCompatActivity {

    Button button_quiz_result;
    private int cool;
    private int warm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_three);

        button_quiz_result = (Button) findViewById(R.id.button_next_three);

        cool = getIntent().getIntExtra("cool", 0);
        warm = getIntent().getIntExtra("warm", 0);

        button_quiz_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioGroup radioGroup = findViewById(R.id.radio_paper);
                int checkedid = radioGroup.getCheckedRadioButtonId();

                switch (checkedid) {
                    case R.id.quiz_blue:
                        cool++;
                        break;
                    case R.id.quiz_green:
                        warm++;
                        break;
                        default:
                            Toast.makeText(QuizActivityThree.this, "선택되지 않았습니다", Toast.LENGTH_SHORT).show();

                            return;
                }


                Intent intent = new Intent(QuizActivityThree.this, ResultTwoActivity.class);
                intent.putExtra("cool", cool);
                intent.putExtra("warm", warm);
                startActivity(intent);
            }
        });

    }
}
