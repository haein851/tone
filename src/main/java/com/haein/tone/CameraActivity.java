package com.haein.tone;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.TextureView;
import android.view.VelocityTracker;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.haein.tone.CapureSave.CaptureSaveAdapter;
import com.haein.tone.cloth.ClothAdapter;
import com.haein.tone.cloth.ClothDecoration;
import com.haein.tone.view.CameraPreview;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class CameraActivity extends Activity {
    private static final int REQUEST_IMAGE_CAPTURE = 672;
    //    private static Camera camera;
    private String imageFilePath;
    private Uri photoUri;
    private final String TAG = CameraActivity.class.getSimpleName();
    public static CameraActivity getInstance;
    private TextureView cameraView;
    private SurfaceHolder holder;
    private ArrayList<String> captureList = new ArrayList<>();
    private Handler mHandler = new Handler();
    private VelocityTracker velocityTracker = null;
    private RecyclerView recyclerView;
    private ImageView iv_color;

    /**캡쳐할 총 레이아웃*/
    private View layout_capture;
    private RecyclerView recycle_album;
    private CaptureSaveAdapter captureSaveAdapter;
    /**캡쳐한 이미지를 올려 놓는 ImageView*/
    private ImageView captureImageView;
    private int[] drawables = {
            R.drawable.autumn1,
            R.drawable.spring1,
            R.drawable.summer1,
            R.drawable.winter1,
            R.drawable.autumn2,
            R.drawable.spring2,
            R.drawable.summer2,
            R.drawable.winter2,
            R.drawable.autumn3,
            R.drawable.spring3,
            R.drawable.summer3,
            R.drawable.winter3,
            R.drawable.autumn4,
            R.drawable.spring4,
            R.drawable.summer4,
            R.drawable.winter4,
            R.drawable.autumn5,
            R.drawable.spring5,
            R.drawable.summer5,
            R.drawable.winter5,
            R.drawable.autumn6,
            R.drawable.spring6,
            R.drawable.summer6,
            R.drawable.winter6,
            R.drawable.autumn7,
            R.drawable.spring7,
            R.drawable.summer7,
            R.drawable.winter7
    };
    private CameraPreview cameraPreviewThread;
//    public static Camera getCamera() {
//        return camera;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        cameraView = findViewById(R.id.preview);
        cameraPreviewThread = new CameraPreview(this, cameraView, true);
        captureImageView = findViewById(R.id.captureImageView);
        recyclerView = findViewById(R.id.recycle_cloth);
        recycle_album = findViewById(R.id.recycle_album);
        Toast.makeText(getApplicationContext(), "어울리는 컬러 5개를 길게 눌러주세요!", Toast.LENGTH_SHORT).show();
        captureSaveAdapter = new CaptureSaveAdapter(this, captureList);
        recycle_album.setAdapter(captureSaveAdapter);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        layout_capture = findViewById(R.id.layout_capture);

        iv_color = findViewById(R.id.iv_color);
        iv_color.setImageBitmap(resizeBitmap(this, R.drawable.autumn1, 360, 616));
        iv_color.setTag(getText(drawables[0]));
        iv_color.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                //TextureView에서 카메라 이미지 가져옴.
                Bitmap bitmap = cameraView.getBitmap();

                BitmapDrawable ob = new BitmapDrawable(getResources(), bitmap);
                captureImageView.setBackground(ob);
                captureImageView.setVisibility(View.VISIBLE);
                
                //Handler를 통해서 캡쳐한 이미지가 등록 후 작업하도록 명령.
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        layout_capture.buildDrawingCache();
                        layout_capture.setDrawingCacheEnabled(true);
                        Bitmap capture_bitmap = layout_capture.getDrawingCache().copy(Bitmap.Config.ARGB_8888, true);
                        layout_capture.destroyDrawingCache();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");
                        Log.i(TAG, "run: "+iv_color.getTag());

                        String[] parsingStringArr = ((String)iv_color.getTag()).split("/");
                        String result = parsingStringArr[parsingStringArr.length-1].replace(".png","");
                        String path = getFilesDir()+ File.separator+result+"_"+simpleDateFormat.format(new Date())+".jpg";
                        try {
                            OutputStream outputStream = new FileOutputStream(path);
                            capture_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        Toast.makeText(getApplicationContext(), path, Toast.LENGTH_SHORT).show();
                        captureList.add(path);
                        captureSaveAdapter.setDrawables(captureList);
                        captureSaveAdapter.notifyDataSetChanged();
                    }
                });
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        captureImageView.setVisibility(View.GONE);
                    }
                }, 1000);
                return true;
            }
        });

        ClothDecoration clothDecoration = new ClothDecoration();

        recyclerView.addItemDecoration(clothDecoration);

        getInstance = this;
        ClothAdapter clothAdapter = new ClothAdapter(CameraActivity.this, drawables, iv_color);

        recyclerView.setAdapter(clothAdapter);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int index = event.getActionIndex();
        int action = event.getActionMasked();
        int pointerId = event.getPointerId(index);

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (velocityTracker == null) {
                    // Retrieve a new VelocityTracker object to watch the
                    // velocity of a motion.
                    velocityTracker = VelocityTracker.obtain();
                } else {
                    // Reset the velocity tracker back to its initial state.
                    velocityTracker.clear();
                }
                // Add a user's movement to the tracker.
                velocityTracker.addMovement(event);
                break;
//            case MotionEvent.ACTION_MOVE:
//                Camera.Parameters cameraParameters = cameraView..getParameters();
//                int maxZoom = cameraParameters.getMaxZoom();
//                int zoom = cameraParameters.getZoom();
//
//                velocityTracker.addMovement(event);
//                Log.i(TAG, "onTouchEvent maxZoom: " + maxZoom);
//                Log.i(TAG, "onTouchEvent zoom: " + zoom);
//                // When you want to determine the velocity, call
//                // computeCurrentVelocity(). Then call getXVelocity()
//                // and getYVelocity() to retrieve the velocity for each pointer ID.
//                velocityTracker.computeCurrentVelocity(1000);
//                // Log velocity of pixels per second
//                // Best practice to use VelocityTrackerCompat where possible.
//                float yVelocity = velocityTracker.getYVelocity();
//                Log.d(TAG, "Y velocity without point: " + yVelocity);
//                if (yVelocity < -1000 && maxZoom >= (zoom + 5)) {
//                    cameraParameters.setZoom(zoom + 5);
//                } else if (yVelocity > 1000) {
//                    if ((zoom - 5) < 0) {
//                        cameraParameters.setZoom(0);
//                    } else {
//                        cameraParameters.setZoom(zoom - 5);
//                    }
//                }
//                try {
//                    cameraView.mCamera.setParameters(cameraParameters);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

//                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                // Return a VelocityTracker object back to be re-used by others.
                velocityTracker.clear();
                break;
        }
        return true;
    }
    public static Bitmap resizeBitmap(Context context, int drawable, int reqWidth, int reqHeight) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(), drawable, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(context.getResources(), drawable, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) <= reqHeight
                    && (halfWidth / inSampleSize) <= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraPreviewThread.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        cameraPreviewThread.onPause();
    }
}

