package com.haein.tone.cloth;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.haein.tone.CameraActivity;
import com.haein.tone.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static com.haein.tone.CameraActivity.resizeBitmap;

public class ClothAdapter extends RecyclerView.Adapter<ClothAdapter.ViewHolder> {

    private final Context context;
    private int[] drawables;
    private ImageView iv_color;

    public ClothAdapter(Context context, int[] drawables, ImageView iv_color) {
        this.drawables = drawables;
        this.context = context;
        this.iv_color = iv_color;
    }

    //처음 recyclerView가 처음 생성될때 item을 생성해 주는 곳.
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_cloth, parent, false);

        return new ViewHolder(view);
    }

    //이미 생성된 item에 값만 바꿔 주는 곳.
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        new BitmapResizeTask(drawables[position], holder.imageView).execute();
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_color.setImageDrawable(holder.imageView.getDrawable());
                iv_color.setTag(context.getText(drawables[position]));
            }
        });
    }

    @Override
    public int getItemCount() {
        return drawables.length;
    }

    //item 한개의 view
    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image_item);

        }

    }

    private class BitmapResizeTask extends AsyncTask<Void, Void, Void> {
        private final ImageView imageView;
        private int drawable;
        private Bitmap bitmap;

        public BitmapResizeTask(int drawable, ImageView imageView){
            this.drawable = drawable;
            this.imageView = imageView;
        }
        @Override
        protected Void doInBackground(Void... voids) {
                bitmap = resizeBitmap(context, drawable, 80, 107);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            imageView.setImageBitmap(bitmap);
        }
    }

}
