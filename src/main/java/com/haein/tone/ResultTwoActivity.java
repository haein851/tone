package com.haein.tone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class ResultTwoActivity extends AppCompatActivity {

    Button btn_resultTwo;
    private int cool;
    private int warm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_two);

        btn_resultTwo = (Button)findViewById(R.id.btn_resultTwo);

        cool = getIntent().getIntExtra("cool", 0);
        warm = getIntent().getIntExtra("warm", 0);

        ImageView imageView = findViewById(R.id.result_image);
        if(cool>warm) {
            imageView.setBackground(getDrawable(R.drawable.quiz_cool));
        } else if(warm>cool) {
            imageView.setBackground(getDrawable(R.drawable.quiz_warm));
        }

        btn_resultTwo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultTwoActivity.this, MenuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

    }
}