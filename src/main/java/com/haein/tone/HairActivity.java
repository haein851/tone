package com.haein.tone;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.haein.tone.Makeup.MakeupAdapter;
import com.haein.tone.Makeup.ProductObject;
import com.haein.tone.hair.HairAdapter;

import java.util.ArrayList;

public class HairActivity extends AppCompatActivity {

    private int[] hair = {5, 3, 4, 4};
    private ArrayList<Integer> hairDrawable = new ArrayList<>();
    private RecyclerView recycle_hairList;
    private ArrayList<ProductObject> productObjs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hair);

        int res = LoginActivity.PERsONAL_COLOR_DBHELPER.toneResult(LoginActivity.ID);
        String resStr = "";
        if(res ==0) resStr="spring";
        else if(res == 1) resStr ="summer";
        else if(res == 2) resStr = "autumn";
        else if(res ==3) resStr = "winter";
        for (int i = 1; i <= hair[res]; i++) {
            int id = getResources().getIdentifier(resStr +"_hair_" + i, "drawable", getPackageName());
            hairDrawable.add(id);
        }
        recycle_hairList = findViewById(R.id.recycle_hairList);
        int array = getResources().getIdentifier(resStr +"_hair", "array", getPackageName());
        String[] hairList = getResources().getStringArray(array);
        for(int i=0;i<hairList.length;i++){
            ProductObject productObject = new ProductObject();

            productObject.drawable = hairDrawable.get(i);
            productObject.name = hairList[i];
            productObjs.add(productObject);
        }

        HairAdapter hairAdapter= new HairAdapter(this, productObjs);

        recycle_hairList = findViewById(R.id.recycle_hairList);
        recycle_hairList.setAdapter(hairAdapter);

    }
}