package com.haein.tone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class QuizActivityOne extends AppCompatActivity {

    private final String TAG = QuizActivityOne.class.getSimpleName();
    Button button_next_one;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_one);

        button_next_one = (Button) findViewById(R.id.button_next_one);

        button_next_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioGroup radioGroup = findViewById(R.id.radio_rip);
                int checkedId = radioGroup.getCheckedRadioButtonId();
                Log.i(TAG, "onClick: ");
                int cool = 0, warm = 0;
                switch (checkedId) {
                    case R.id.quiz_pink:
                        cool++;
                        break;
                    case R.id.quiz_red:
                        warm++;
                        break;
                    default:
                        Toast.makeText(QuizActivityOne.this, "선택되지 않았습니다", Toast.LENGTH_SHORT).show();
                        return;
                }
                Intent intent = new Intent(QuizActivityOne.this, QuizActivityTwo.class);
                intent.putExtra("cool", cool);
                intent.putExtra("warm", warm);
                startActivity(intent);
            }
        });


    }
}
